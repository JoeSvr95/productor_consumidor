#include "csapp.h"
#define SIZE 10

typedef struct{
	char *buf;
	int n;
	int front;
	int rear;
	sem_t mutex;
	sem_t slots;
	sem_t items;
} sbuf_t;

void sbuf_deinit(sbuf_t *sp);
void sbuf_init(sbuf_t *sp, int n);
void sbuf_insert(sbuf_t *sp, char item);
void *read_character(void *descriptor);
void *print_character(void *arg);
char sbuf_remove(sbuf_t *sp);

sbuf_t sbuffer;
int fd;

int main(int argc, char **argv)
{
	char *filename;
	struct stat fileStat;
    	pthread_t tid1,tid2;
	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(0);
	}
	filename = argv[1];


	if (stat(filename, &fileStat)<0){	
		printf("Archivo %s no fue encontrado\n",filename);
	}
	else{
        sbuf_init(&sbuffer,SIZE);
	printf("Abriendo archivo %s...\n",filename);
        fd = Open(filename, O_RDONLY, 0);
	Pthread_create(&tid1,NULL,read_character,NULL);
        Pthread_create(&tid2,NULL,print_character,NULL);
        Pthread_join(tid1,NULL);
        Pthread_join(tid2,NULL);	
		Close(fd);
		sbuf_deinit(&sbuffer);
	}
	
	return 0;
}
void *read_character(void *descriptor){
    char c;
    while(Read(fd,&c,1)){
			usleep(50000);
			sbuf_insert(&sbuffer,c);
    }
    return NULL;
}

void *print_character(void *arg){
    do{
	sleep(1);
        char s= sbuf_remove(&sbuffer);
        printf("%c\n",s);
    }while(sbuffer.front!=sbuffer.rear);
    return NULL;
}

void sbuf_init(sbuf_t *sp, int n){
	sp->buf = Calloc(n, sizeof(char));
	sp->n = n;
	sp->front = sp->rear = 0;
	Sem_init(&sp->mutex, 0, 1);
	Sem_init(&sp->slots, 0, n);
	Sem_init(&sp->items, 0, 0);
}

void sbuf_insert(sbuf_t *sp, char item){
	P(&sp->slots);
	P(&sp->mutex);
	sp->buf[(++sp->rear)%(sp->n)] = item;
	V(&sp->mutex);
	V(&sp->items);
}

char sbuf_remove(sbuf_t *sp){
	char item;
	P(&sp->items);
	P(&sp->mutex);
	item = sp->buf[(++sp->front)%(sp->n)];
	V(&sp->mutex);
	V(&sp->slots);
	return item;
}

void sbuf_deinit(sbuf_t *sp){
	Free(sp->buf);
}
